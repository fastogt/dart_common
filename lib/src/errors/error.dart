class ErrorJson {
  static const String CODE_FIELD = 'code';
  static const String MESSAGE_FIELD = 'message';
  static const String DATA_FIELD = 'data';

  final int code;
  final String message;
  final dynamic data;

  ErrorJson(this.code, this.message, this.data);

  Map<String, dynamic> toJson() {
    final result = {CODE_FIELD: code, MESSAGE_FIELD: message};
    if (data != null) {
      result[DATA_FIELD] = data;
    }
    return result;
  }

  factory ErrorJson.fromJson(Map<String, dynamic> json) {
    final code = json[CODE_FIELD];
    final message = json[MESSAGE_FIELD];
    dynamic data;
    if (json.containsKey(DATA_FIELD)) {
      data = json[DATA_FIELD];
    }
    return ErrorJson(code, message, data);
  }
}

abstract class IError {
  String error();
}

class ErrorHttp extends IError {
  final int statusCode;
  final String? reason;
  final dynamic body;

  ErrorHttp(this.statusCode, this.reason, [this.body]) : super();

  @override
  String error() {
    if (body != null) {
      return body.toString();
    }
    return 'Unknown error';
  }

  bool isErrorJson() {
    return body is ErrorJson;
  }

  ErrorJson errorJson() {
    return body as ErrorJson;
  }
}
