import 'dart:async';
import 'dart:convert';

import 'package:dart_common/errors.dart';
import 'package:http/http.dart';

Future<Response> handleResponse(Future<Response> future, List<int> statusCodes) {
  return handleBaseResponse<Response>(future, statusCodes);
}

Future<StreamedResponse> handleStreamedResponse(
    Future<StreamedResponse> future, List<int> statusCodes) {
  return handleBaseResponse<StreamedResponse>(future, statusCodes);
}

Future<T> handleBaseResponse<T extends BaseResponse>(Future<T> future, List<int> statusCodes) {
  final completer = Completer<T>();
  future.then((resp) {
    final bool _checkCode = _checkStatusCode(resp.statusCode, statusCodes);
    if (_checkCode) {
      return completer.complete(resp);
    }
    final ErrorHttp errorHttp = _makeError(resp);
    return completer.completeError(errorHttp);
  }, onError: (e) {
    final error = ErrorHttp(502, 'Bad Gateway', e.toString());
    return completer.completeError(error);
  });

  return completer.future;
}

// private:
ErrorHttp _errorFromResponse(Response resp) {
  Map<String, dynamic> data = {};
  try {
    data = json.decode(resp.body);
    try {
      final errj = ErrorJson.fromJson(data['error']);
      return ErrorHttp(resp.statusCode, resp.reasonPhrase, errj);
      // ignore: empty_catches
    } catch (e) {}

    final err = data['error'];
    return ErrorHttp(resp.statusCode, resp.reasonPhrase, err);
  } catch (e) {
    return ErrorHttp(resp.statusCode, resp.reasonPhrase, resp.body);
  }
}

ErrorHttp _errorFromStreamedResponse(StreamedResponse resp) {
  return ErrorHttp(resp.statusCode, resp.reasonPhrase);
}

bool _checkStatusCode(int current, List<int> allowed) {
  for (final int code in allowed) {
    if (code == current) {
      return true;
    }
  }

  return false;
}

ErrorHttp _makeError(BaseResponse resp) {
  if (resp is StreamedResponse) {
    return _errorFromStreamedResponse(resp);
  }
  return _errorFromResponse(resp as Response);
}
